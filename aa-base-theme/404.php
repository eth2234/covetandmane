<?php
get_header();
?>
	
	<div class="container">
		<div class="content">
			<article <?php post_class('not-found'); ?>>
				<?php the_field( 'error_page_text', 'option' ); ?>
				<?php get_search_form(); ?>
			</article>
		</div>
	</div>

<?php
get_footer();