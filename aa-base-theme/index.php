<?php
get_header();
?>
	<div class="container">
		<div class="content-area">
			<?php
			while( have_posts() ): the_post();
				
				get_template_part( '_template-parts/loop-post', get_post_type() );
			
			endwhile;
			
			get_template_part( '_template-parts/share-links' );
			
			get_template_part( '_template-parts/post-author' );
			
			get_template_part( '_template-parts/related-posts' );
			
			get_template_part( '_template-parts/page-navigation' );
			
			comments_template();
			?>
		</div>
		
		<div class="aside">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php
get_footer();