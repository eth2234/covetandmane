<article <?php post_class( 'entry entry-archive' ); ?>>
	
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="entry-image">
			<a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image( get_post_thumbnail_id(), 'full' ); ?></a>
		</div>
	<?php } ?>
	
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>
	
	<?php get_template_part( '_template-parts/post-meta' ); ?>
	
	<div class="entry-content">
		<?php the_excerpt(__('new_excerpt_length')); ?>
	</div>

	<div class="entry-read-more">
		<a href="<?php the_permalink(); ?>" class="button alt">Read More</a>
	</div>

</article>