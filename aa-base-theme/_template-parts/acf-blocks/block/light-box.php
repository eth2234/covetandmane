<?php
/**
 * Block Name: Lightbox Gallery Content
 *
 * This is the template that displays the ACF general content block.
 */

?>	
<style>
	.gallery{
		display:flex;
		flex-wrap: wrap;
		justify-content: space-between;
		max-width:960px;
		margin:0 auto;
	}
	.gallery-img{
		min-width:150px;
	}
	
</style>
<div class="container">
	<div class="content">
<?php 
$images = get_field('gallery');
//$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
if( $images ): ?>

        <div class="gallery">
            <?php foreach( $images as $image ): ?>
               <div class="gallery-img">
					<a data-fancybox="images" href="<?php echo esc_url($image['url']); ?>">
                	<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
            	</a>
			</div>	

            <?php endforeach; ?>
		</div>

<?php endif; ?>
	</div>
</div>