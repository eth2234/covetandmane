<?php
/**
 * Block Name: Columns
 *
 * This is the template that displays the ACF general content block.
 */

?>	
<div class="container">
	<div class="content">
			<?php the_field('column_one'); ?>
			<?php the_field('column_two'); ?>
	</div>
</div>