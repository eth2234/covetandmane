<article <?php post_class( 'entry entry-single' ); ?>>
	
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="entry-image">
			<?php echo wp_get_attachment_image( get_post_thumbnail_id(), 'full' ); ?>
		</div>
	<?php } ?>
	
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header>
	
	<?php get_template_part( '_template-parts/post-meta' ); ?>
	
	<div class="entry-content">
		<?php the_content(); ?>
	</div>

</article>