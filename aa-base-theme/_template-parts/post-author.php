<?php if ( is_single() ) : 
$auth_bio = get_the_author_meta('description'); 
		if( !empty($auth_bio) ): ?>
		<!--BEGIN AUTHOR BIO-->
		<div class='clearfix' id='about_author'>
			<?php echo get_avatar( get_the_author_meta('email'), '150' ); ?>
			<div class='author_text'>
				<h4>About <a href='<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>'><?php echo get_the_author_meta("first_name"); ?> <?php echo get_the_author_meta("last_name"); ?></a></h4>
				<p class="author-description"><?php the_author_meta('description'); ?></p>
				<div class='clear'></div>	
			</div>
		</div>
        	<!--END AUTHOR BIO-->
<?php endif;
endif; ?>