<?php
if ( $area = aa_get_sidebar() ) {
	
	$classes = array( 'sidebar' );
	$classes[] = ($area == 'sidebar') ? 'sidebar-default' : 'sidebar-' . $area;
	?>
	<aside id="sidebar" class="<?php echo esc_attr(implode(' ', $classes)); ?>">
		<?php dynamic_sidebar( $area ); ?>
	</aside>
	<?php
}