<!DOCTYPE html>
<html lang="en">
<head>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div>
	
	<div id="outer-wrapper">
		
		
		<!-- Begin Header -->
		<header>
			<div id="header-wrapper">
				<div class="header-inner container">
					<?php
					$image = function_exists('get_field')  ? get_field( 'site_header_logo', 'option' ) : false;
					if ( $image && !empty($image['url']) ) {
						echo '<a href="', esc_attr(site_url()), '" target="_self"><img class="logo" src="', esc_attr($image['url']), '" alt="', esc_attr(get_bloginfo('name')), '" /></a>';
					}else{
						echo '<a href="', esc_attr(site_url()), '" target="_self"><img class="logo" src="https://alchemyandaim.com/wp-content/uploads/2016/06/alchemy-and-aim-logo.png" alt="', esc_attr(get_bloginfo('name')), '" /></a>';
					}
					?>
					
					<!-- Begin Header -->
					<div id="linkbar" class="nav-bar">
						<nav id="menu" class="nav-menu" role="navigation">
							<?php wp_nav_menu( array(
								'theme_location' => 'primary',
								'container'      => '',
								'container_id'   => '',
								'menu_class'     => '',
								'item_spacing'   => 'discard',
								'items_wrap'     => '<ul class="nav-list">%3$s</ul>',
							) ); ?>
						</nav>
						<a href="#" id="slideout-trigger">
							<div id="slideout-bar"></div>
						</a>
					</div>
				</div>
			</div><!--HEADER WRAPPER-->
		</header>
		
		<div id="slideout-menu">
			<nav id="slideout-nav">
				<?php wp_nav_menu( array( 'theme_location' => 'mobile' ) ); ?>
				<a href="#" id="nav-close">
					<div class="close-lines1"></div>
					<div class="close-lines2"></div>
				</a>
			</nav>
		</div>
		
		<div id="content-wrapper">