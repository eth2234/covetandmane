<?php

/*---------------------------------------
	Basic theme configuration
-----------------------------------------*/
function aa_theme_setup() {
	// 1. Theme Features
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	add_theme_support( 'editor-styles' );
	add_theme_support( 'align-wide' );
	
	// 2. Menus
	$menus = array(
		'primary' => 'Primary',
		'mobile' => 'Mobile',
	);
	register_nav_menus($menus);
	
	// 3. Sidebars
	$sidebars = array(
		'sidebar'    => array(
			'Sidebar',
			'Default sidebar.',
		),
	);
	
	foreach ( $sidebars as $key => $bar ) {
		register_sidebar( array(
			'id'          => $key,
			'name'        => $bar[0],
			'description' => $bar[1],
			
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>'
		));
	}
	
	if ( function_exists( 'acf_add_options_page' ) ) {
		if ( have_rows( 'register_sidebar', 'options' ) ) while( have_rows( 'register_sidebar', 'options' ) ): the_row();
			register_sidebar( array(
				'name'          => get_sub_field( 'title' ),
				'id'            => get_sub_field( 'id' ),
				'description'   => '',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
			) );
		endwhile;
	}
	
	// 4. Shortcodes
	add_shortcode( 'year', function () { return date('Y'); } );
	
	// 5. Custom image sizes
	// add_image_size( 'featured-thumbnail', 150, 150, true );
	
	// 6. Add editor stylesheet (To add TypeKit styles edit _static/js/editor.js and add the typekit id there.)
	add_editor_style( get_template_directory_uri() . '/_static/styles/_admin/editor-styles.min.css' );
	
}
add_action( 'after_setup_theme', 'aa_theme_setup' );

/*---------------------------------------
 	Enqueue block editor style
-----------------------------------------*/

function gutenberg_editor_assets() {
  // Load the theme styles within Gutenberg.
  wp_enqueue_style('gutenberg-editor-styles', get_template_directory_uri() .  '/_static/styles/_admin/gutenberg-editor-styles.min.css', FALSE);
}
add_action('enqueue_block_editor_assets', 'gutenberg_editor_assets');

// Add theme scripts/styles
function aa_enqueue_scripts() {
	$version = wp_get_theme()->Version;
	
	/******************************
	 * OPTIONAL FILES
	 ******************************/
	// Web fonts
	// wp_enqueue_style( 'font-montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:400,700' );
	
	// Font Awesome
	wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css' );
	wp_enqueue_script( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js', array(), '5.13.0' );
	wp_enqueue_script( 'font-awesome-4-shim', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/v4-shims.min.js', array(), '5.13.0' );
	
	// Modernizr
	wp_enqueue_script( 'modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array(), '2.8.3', true );
	
	// Browser detection and browser-specific stylesheet
	wp_enqueue_script( 'browser-detect', get_template_directory_uri() . '/_static/js/browser-detect.js', array(), '1.0', true );
	
	// ACF + Yoast script
	wp_enqueue_script( 'acf_seo', get_template_directory_uri() . '/_static/js/acf-yoast.js', array( 'jquery' ), '1.0.0', true );
	
	// Normalize.css
	wp_enqueue_style( 'normalize', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css', array(), '7.0.0' );
	
	// Smooth Scroll
	wp_enqueue_script( 'smooth-scroll', 'https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll/dist/smooth-scroll.polyfills.min.js', array(), '16.0.2', true );
	
	
	/******************************
	 * THEME FILES
	 ******************************/
	wp_enqueue_script( 'main', get_template_directory_uri() . '/_static/js/main.js', array( 'jquery' ), $version, true );
	
	wp_enqueue_style( 'stylesheet', get_stylesheet_uri(), array(), $version );
	wp_enqueue_style( 'vanilla-styles', get_stylesheet_directory_uri() . '/vanilla-style.css', array(), $version );
}
add_action( 'wp_enqueue_scripts', 'aa_enqueue_scripts' );


// Add admin scripts/styles
function aa_admin_enqueue_scripts() {
	$version = wp_get_theme()->Version;
	
	/******************************
	 * THEME FILES BACKEND
	 ******************************/
	wp_enqueue_style( 'aa-admin', get_template_directory_uri() . '/_static/styles/_admin/admin-styles.min.css', array(), $version );
	wp_enqueue_script( 'aa-admin', get_template_directory_uri() . '/_static/js/admin.js', array('jquery'), $version );
}
add_action( 'admin_enqueue_scripts', 'aa_admin_enqueue_scripts' );


/*---------------------------------
	Custom Local Avatars
------------------------------------*/

add_filter( 'get_avatar', 'tsm_acf_profile_avatar', 10, 5 );
function tsm_acf_profile_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
	
	// Get user by id or email
	if ( is_numeric( $id_or_email ) ) {
		
		$id = (int)$id_or_email;
		$user = get_user_by( 'id', $id );
		
	}elseif ( is_object( $id_or_email ) ){
		
		if ( !empty( $id_or_email->user_id ) ) {
			$id = (int)$id_or_email->user_id;
			$user = get_user_by( 'id', $id );
		}
		
	}else{
		$user = get_user_by( 'email', $id_or_email );
	}
	
	// Get the user id
	$user_id = $user->ID;
	
	// Get the file id
	$image_id = get_user_meta( $user_id, 'tsm_local_avatar', true ); // CHANGE TO YOUR FIELD NAME
	
	// Bail if we don't have a local avatar
	if ( !$image_id ) {
		return $avatar;
	}
	
	// Get the file size
	$image_url = wp_get_attachment_image_src( $image_id, 'thumbnail' ); // Set image size by name
	// Get the file url
	$avatar_url = $image_url[0];
	// Get the img markup
	$avatar = '<img alt="' . $alt . '" src="' . $avatar_url . '" class="avatar avatar-' . $size . '" height="' . $size . '" width="' . $size . '"/>';
	
	// Return our new avatar
	return $avatar;
}


/*---------------------------------
	Feature to RSS
------------------------------------*/

function featuredtoRSS( $content ) {
	global $post;
	if ( has_post_thumbnail( $post->ID ) ) {
		$content = '' . get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'margin:10px auto;display:block;' ) ) . '' . $content;
	}
	
	return $content;
}

add_filter( 'the_excerpt_rss', 'featuredtoRSS' );
add_filter( 'the_content_feed', 'featuredtoRSS' );

/*-----------------------------------------------
	Add Featured Image Column for Posts & Pages
--------------------------------------------------*/

add_filter( 'manage_posts_columns', 'add_thumbnail_column', 5 );

function add_thumbnail_column( $columns ) {
	$columns['new_post_thumb'] = __( 'Featured Image' );
	
	return $columns;
}

add_action( 'manage_posts_custom_column', 'display_thumbnail_column', 5, 2 );

function display_thumbnail_column( $column_name, $post_id ) {
	switch( $column_name ) {
		case 'new_post_thumb':
			$post_thumbnail_id = get_post_thumbnail_id( $post_id );
			if ( $post_thumbnail_id ) {
				$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );
				echo '<img width="150" src="' . $post_thumbnail_img[0] . '" />';
			}
			break;
	}
}

add_filter( 'manage_pages_columns', 'custom_pages_columns' );
function custom_pages_columns( $columns ) {
	
	$columns['new_page_thumb'] = __( 'Featured Image' );
	
	return $columns;
}

add_action( 'manage_pages_custom_column', 'custom_page_column_content', 10, 2 );

function custom_page_column_content( $column_name, $post_id ) {
	switch( $column_name ) {
		case 'new_page_thumb':
			$post_thumbnail_id = get_post_thumbnail_id( $post_id );
			if ( $post_thumbnail_id ) {
				$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );
				echo '<img width="150" src="' . $post_thumbnail_img[0] . '" />';
			}
			break;
	}
}

/* ------------------------
-----   Show Page/Post IDs    -----
------------------------------*/

/*Forked from "Simply Show IDs" Plugin*/
// Prepend the new column to the columns array
function ssid_column( $cols ) {
	$cols['ssid'] = 'ID';
	
	return $cols;
}

// Echo the ID for the new column
function ssid_value( $column_name, $id ) {
	if ( $column_name == 'ssid' ) {
		echo $id;
	}
}

function ssid_return_value( $value, $column_name, $id ) {
	if ( $column_name == 'ssid' ) {
		$value = $id;
	}
	
	return $value;
}

// Output CSS for width of new column
function ssid_css() {
	?>
	<style type="text/css">
		#ssid {
			width: 50px;
		}
		
		/* Simply Show IDs */
	</style>
	<?php
}

// Actions/Filters for various tables and the css output
function ssid_add() {
	add_action( 'admin_head', 'ssid_css' );
	
	add_filter( 'manage_posts_columns', 'ssid_column' );
	add_action( 'manage_posts_custom_column', 'ssid_value', 10, 2 );
	
	add_filter( 'manage_pages_columns', 'ssid_column' );
	add_action( 'manage_pages_custom_column', 'ssid_value', 10, 2 );
	
	add_filter( 'manage_media_columns', 'ssid_column' );
	add_action( 'manage_media_custom_column', 'ssid_value', 10, 2 );
	
	add_filter( 'manage_link-manager_columns', 'ssid_column' );
	add_action( 'manage_link_custom_column', 'ssid_value', 10, 2 );
	
	add_action( 'manage_edit-link-categories_columns', 'ssid_column' );
	add_filter( 'manage_link_categories_custom_column', 'ssid_return_value', 10, 3 );
	
	foreach( get_taxonomies() as $taxonomy ) {
		add_action( "manage_edit-${taxonomy}_columns", 'ssid_column' );
		add_filter( "manage_${taxonomy}_custom_column", 'ssid_return_value', 10, 3 );
	}
	
	add_action( 'manage_users_columns', 'ssid_column' );
	add_filter( 'manage_users_custom_column', 'ssid_return_value', 10, 3 );
	
	add_action( 'manage_edit-comments_columns', 'ssid_column' );
	add_action( 'manage_comments_custom_column', 'ssid_value', 10, 2 );
}

add_action( 'admin_init', 'ssid_add' );

/*---------------------------------
	A custom pagination class
------------------------------------*/

add_filter( 'next_posts_link_attributes', 'posts_link_attributes' );
add_filter( 'previous_posts_link_attributes', 'posts_link_attributes' );

function posts_link_attributes() {
	return 'class="button button-primary"';
}


add_filter( 'next_post_link', 'post_link_attributes' );
add_filter( 'previous_post_link', 'post_link_attributes' );

function post_link_attributes( $output ) {
	$injection = 'class="button button-primary"';
	
	return str_replace( '<a href=', '<a ' . $injection . ' href=', $output );
}

/*---------------------------------
	Deal with WP empty paragraphs
------------------------------------*/

function formatter( $content ) {
	
	$bad_content = array(
		'<p></div></p>',
		'<p><div class="full',
		'_width"></p>',
		'</div></p>',
		'<p><ul',
		'</ul></p>',
		'<p><div',
		'<p><block',
		'quote></p>',
		'<p><hr /></p>',
		'<p><table>',
		'<td></p>',
		'<p></td>',
		'</table></p>',
		'<p></div>',
		'nosidebar"></p>',
		'<p><p>',
		'<p><a',
		'</a></p>',
		'-half"></p>',
		'-third"></p>',
		'-fourth"></p>',
		'<p><p',
		'</p></p>',
		'child"></p>',
		'<p></p>',
		'-fifth"></p>',
		'-sixth"></p>',
		'last"></p>',
		'fix" /></p>',
		'<p><hr',
		'<p><li',
		'"centered"></p>',
		'</li></p>',
		'<div></p>',
		'<p></ul>',
		'<p><img',
		' /></p>',
		'"nop"></p>',
		'tures"></p>',
		'"left"></p>',
		'<p><h1 class="center">',
		'centered"></p>',
	);
	$good_content = array(
		'</div>',
		'<div class="full',
		'_width">',
		'</div>',
		'<ul',
		'</ul>',
		'<div',
		'<block',
		'quote>',
		'<hr />',
		'<table>',
		'<td>',
		'</td>',
		'</table>',
		'</div>',
		'nosidebar">',
		'<p>',
		'<a',
		'</a>',
		'-half">',
		'-third">',
		'-fourth">',
		'<p',
		'</p>',
		'child">',
		'',
		'-fifth">',
		'-sixth">',
		'last">',
		'fix" />',
		'<hr',
		'<li',
		'"centered">',
		'</li>',
		'<div>',
		'</ul>',
		'<img',
		' />',
		'"nop">',
		'tures">',
		'"left">',
		'<h1 class="center">',
		'centered">',
	);
	
	$new_content = str_replace( $bad_content, $good_content, $content );
	
	return $new_content;
	
}

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop', 10 );
add_filter( 'the_content', 'formatter', 11 );

/*---------------------------------
	Thumbnail Size Upscale
------------------------------------*/
function alx_thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ) {
	if ( !$crop ) return null; // let the wordpress default function handle this
	
	$size_ratio = max( $new_w / $orig_w, $new_h / $orig_h );
	
	$crop_w = round( $new_w / $size_ratio );
	$crop_h = round( $new_h / $size_ratio );
	
	$s_x = floor( ( $orig_w - $crop_w ) / 2 );
	$s_y = floor( ( $orig_h - $crop_h ) / 2 );
	
	return array(
		0,
		0,
		(int)$s_x,
		(int)$s_y,
		(int)$new_w,
		(int)$new_h,
		(int)$crop_w,
		(int)$crop_h,
	);
}

add_filter( 'image_resize_dimensions', 'alx_thumbnail_upscale', 10, 6 );

/*------------------------------------------------
	Page Slug Body Class
---------------------------------------------------*/
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	
	return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );


/*------------------------------------------------
	Gravity Forms Hide Labels Filter
---------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*---------------------------------
	Disable XMLRPC
------------------------------------*/
add_filter( 'xmlrpc_enabled', '__return_false' );

/*---------------------------------
	Load Common Functions
------------------------------------*/
require_once( '_includes/common-functions.php' );