<?php
/*
Template Name: Flex Layout
*/

$sections = get_field( 'flex_sections' );

get_header();
?>

<div class="flex-sections">
	<?php
	while( have_posts() ): the_post();
	
		// Loop through flexible fields and load the respective file for each.
		foreach( $sections as $i => $section ) {
			$part = get_template_directory() . '/_template-parts/flex-layout/sections/' . $section['acf_fc_layout'] . '.php';
			
			if ( file_exists( $part ) ) {
				include( $part );
			}else{
				echo '<!-- Error: No such flexible field type "'. esc_html($section['acf_fc_layout']) .' at '. esc_html($part) .'" -->';
			}
			
		}
	
	endwhile;
	?>
</div>

<?php
get_footer();