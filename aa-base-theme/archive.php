<?php
if ( !have_posts() ) {
	// If no posts match the query
	get_template_part( '404' );
	return;
}

get_header();
?>
	<div class="container">
		<div class="content-area">
			<?php if (is_home() ) { ?>
			<h3 class="archive-header">Blog</h3>
			<?php } else { ?>
			<h3 class="archive-header"><?php the_archive_title(); ?></h3>
			<?php } ?>
			
			<?php
			while( have_posts() ): the_post();
				get_template_part( '_template-parts/loop-archive', get_post_type() );
			endwhile;
			?>
			
			<?php get_template_part( '_template-parts/page-navigation' ); ?>
		</div>
		
		<div class="aside">
			<?php get_sidebar(); ?>
		</div>
	</div>
<?php
get_footer();