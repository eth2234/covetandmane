<?php
// If using a search page, redirect to the top level search to continue the query (see _includes/plugins/search-page.php), but still use the search page for content
$search_page_id = get_option( 'page_for_search' );

if ( is_page() && get_the_ID() === (int) $search_page_id ) {
	wp_redirect( add_query_arg( array('s' => get_query_var('s')), site_url() ) );
	exit;
}

global $wp_query;
$search_page = get_post( $search_page_id );
$title = $search_page ? $search_page->post_title : 'Search';
$s = trim(get_query_var('s'));

get_header();
?>
<div class="container">
	<div class="content-area">
		<?php if ( have_posts() && $s ) : ?>
			
			<h1 class="entry-title"><?php echo $title; ?></h1>
			
			<div class="entry-content">
				<p>Found <?php printf(_n('%d result', '%d results', $wp_query->found_posts), $wp_query->found_posts, $wp_query->found_posts ); ?> for the search term &ldquo;<?php echo esc_html( $s ); ?>&rdquo;.</p>
				<?php get_search_form(); ?>
			</div>
			
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( '_template-parts/loop-archive', get_post_type() );
			endwhile; // End of the loop.
			
			if ( $wp_query->found_posts > count($wp_query->posts) ) {
				get_template_part( '_template-parts/page-navigation' );
			}
			?>
		
		<?php else : ?>
			
			<h1 class="entry-title"><?php echo $title; ?></h1>
			
			<div class="entry-content">
				<p>Enter search keywords below to begin.</p>
				<?php get_search_form(); ?>
			</div>
		
		<?php endif; ?>
	</div>
	
	<div class="aside">
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>
