<?php

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM POST TYPE REGISTRATION
/*-----------------------------------------------------------------------------------*/

// Create Custom Post Type
function cpt_init() {
    $args = array(
      'label' => 'CPT',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'cpt'),
        'query_var' => true,
        'menu_icon' => 'dashicons-admin-post',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'comments',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',)
        );
    register_post_type( 'cpt', $args );
}
add_action( 'init', 'cpt_init' );

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM TAXONOMIES
/*-----------------------------------------------------------------------------------*/

function cpt_taxonomies() {
  $labels = array(
    'name'              => _x( 'CPT Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'CPT Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search CPT Categories' ),
    'all_items'         => __( 'All CPT Categories' ),
    'parent_item'       => __( 'Parent CPT Category' ),
    'parent_item_colon' => __( 'Parent CPT Category:' ),
    'edit_item'         => __( 'Edit CPT Category' ), 
    'update_item'       => __( 'Update CPT Category' ),
    'add_new_item'      => __( 'Add New CPT Category' ),
    'new_item_name'     => __( 'New CPT Category' ),
    'menu_name'         => __( 'CPT Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'video_category', 'cpt', $args );
}
add_action( 'init', 'cpt_taxonomies', 0 );

/* THIS HELPS FOR SINGLE PAGES FOR CUSTOM POST TYPES */
flush_rewrite_rules();