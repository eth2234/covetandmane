<?php
/*---------------------------------
	Theme functions
------------------------------------*/
require_once( 'functions/admin.php' ); // Customized admin features
require_once( 'functions/editor.php' ); // Visual editor customizations
require_once( 'functions/welcome-panel.php' ); // Add a custom welcome panel on the back end.
require_once( 'functions/theme-functions.php' ); // Functions used in the theme to be called directly
require_once( 'functions/theme-hooks.php' ); // Hooks tied to the theme, these shouldn't be called directly

//Enable ACF Gutenburg Block Builder
if ( function_exists( 'acf_add_options_page' ) && !empty( get_option( 'options_use_gutenburg_block_builder' ) ) ) { 
	require_once( 'functions/acf-block-functions.php' ); // Add functionality for ACF Gutenburg Block Builder
} else {
	add_action('admin_head', 'hide_gutenburg_block_builder');
	function hide_gutenburg_block_builder() {
  	echo '<style>
    	#acf-group_5c9d327d28a8f{display:none !important;} 
  		</style>';
	}
}

//Enable WooCommerce functions ONLY if WooCommerce exists.
if ( in_array( 'woocommerce/woocommerce.php', 
    apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) 
  )) {
			
		require_once( 'functions/woocommerce.php' ); // Enable woocommerce support, and custom woocommerce functions
	
		function create_sidebars() {
			register_sidebar( array(
				'name' => 'Store',
				'id' => 'store_sidebar',
				'description' => 'WooCommerce store sidebar.',

				'before_widget' => '<div id="%1$s" class="widget sidebox %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>',
			));
			register_sidebar( array(
				'name' => 'Checkout',
				'id' => 'checkout_sidebar',
				'description' => 'WooCommerce checkout page sidebar.',

				'before_widget' => '<div id="%1$s" class="widget sidebox %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>',
			));
		}
		add_action( 'after_setup_theme', 'create_sidebars' );
}

//Enable Flex Template
if ( function_exists( 'acf_add_options_page' ) &&  get_field( 'use_flex_layout', 'option' ) == 1 ) { 
 require_once( 'functions/flex-layout-functions.php' ); // Add functionality for Radley's flex layout system
} else {
	function remove_flex_page_template() {
    global $pagenow;
    if ( in_array( $pagenow, array( 'post-new.php', 'post.php') ) && get_post_type() == 'page' ) { ?>
        <script type="text/javascript">
            (function($){
                $(document).ready(function(){
                    $('#page_template option[value="_templates/template_flex_layout_page.php"]').remove();
                })
            })(jQuery)
        </script>
    <?php 
    }
}
add_action('admin_footer', 'remove_flex_page_template', 10);
}

/*---------------------------------
	Widgets
------------------------------------*/
if ( function_exists( 'acf_add_options_page' ) && get_field( 'use_social_links', 'option' ) == 1 ) {
	require_once( 'widgets/social-widget.php' );
}

/*---------------------------------------------
	Custom Post Types
------------------------------------------------*/
// require_once( 'cpts/custom-post-type-template.php' );

/*---------------------------------------------
	Advanced Custom Fields: Shortcode Support
------------------------------------------------*/
//Add shortcode support to ACF text, textarea & medium editor fields
add_filter('acf/format_value/type=medium_editor', 'do_shortcode');
add_filter('acf/format_value/type=textarea', 'do_shortcode');
add_filter('acf/format_value/type=text', 'do_shortcode');

/*---------------------------------------------
	Advanced Custom Fields: Theme Settings
------------------------------------------------*/

if ( function_exists( 'acf_add_options_page' ) ) {
	$user = wp_get_current_user();
	$user = $user->user_login;
	
	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false,
	) );
	
	if ( $user == "alchemyandaim" ):
		acf_add_options_sub_page( array(
			'page_title'  => 'Theme Developer Page',
			'menu_title'  => 'Developer',
			'parent_slug' => 'theme-general-settings',
		) );
	
	endif;
	
	if($user != "alchemyandaim"): 	
		add_filter('acf/settings/show_admin', '__return_false');
	endif;
	
	if($user != "alchemyandaim"):
	//Hide Advanced Tab in Flexlayout
	add_action('admin_head', 'hide_advanced_tab');

	function hide_advanced_tab() {
  	echo '<style>
    	.acf-field-5b243d974f1b6{display:none;} 
  		</style>';
	}
	endif;
}
/*---------------------------------
	Add custom dashboard widgets defined in ACF
------------------------------------*/

function aa_accordion_dashboard_widget() {
	if ( !function_exists( 'acf_add_options_page' ) ) return;
global $wp_meta_boxes;
	
$accordion = get_field( 'accordion_dashboard_widget', 'option' );
$widget_title = get_field( 'accordion_dashboard_widget_title', 'option' );
	
	if($accordion == 1){
 
wp_add_dashboard_widget('accordion_dashboard_widget', $widget_title, 'custom_dashboard_help');
}


function custom_dashboard_help() { 
	
	if ( have_rows( 'accordion_widget', 'option' ) ) : 
	while ( have_rows( 'accordion_widget', 'option' ) ) : the_row(); 
	
		//vars
		$label = get_sub_field( 'accordion_label' ); 
		$content = get_sub_field( 'accordion_content' );
	
	echo '<div class="accordion">'.$label.'</div>';
	echo '<div class="panel">'.$content.'</div>';
	
	 endwhile; 
 else : 
	// no rows found 
endif; 

	echo '<script>
	var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>';
 }
	}

add_action('wp_dashboard_setup', 'aa_accordion_dashboard_widget');

function aa_dashboard_widget() {
	if ( !function_exists( 'acf_add_options_page' ) ) return;
	
	if ( have_rows( 'dashboard_widgets', 'options' ) ):
		while( have_rows( 'dashboard_widgets', 'options' ) ): the_row();
			
			// vars
			$title = get_sub_field( 'widget_title' );
			$name_clean = sanitize_title( $title );
			$the_msg = get_sub_field( 'widget_content' );
			
			wp_add_dashboard_widget(
				$name_clean . '_dashboard_widget',     // Widget slug
				$title,                              // Title
				'dashboard_widget_display_function', // Display function
				'dashboard_widget_control_function', // Control function
				array(
					'title'   => $title,
					'slug'    => $name_clean,
					'content' => $the_msg,
				) // Callback arguments
			);
		
		endwhile;
	endif;
}

add_action( 'wp_dashboard_setup', 'aa_dashboard_widget' );

function dashboard_widget_display_function( $post_obj, $args ) {
	printf( $args['args']['content'] );
}

/*---------------------------------
	Add default page with formatted content for testing typography in the theme
------------------------------------*/

if ( isset( $_GET['activated'] ) && is_admin() ) {
	$new_page_title = 'Default Page';
	$new_page_content = '<!-- Sample Content to Plugin to Template -->

The purpose of this HTML is to help determine what default settings are with CSS and to make sure that all possible HTML Elements are included in this HTML so as to not miss any possible Elements when designing a site.

<hr />

<div class="grid grid-3-col">
<div class="cell">
<h2 id="headings">Headings</h2>
<h1>Heading 1</h1>
<h2>Heading 2</h2>
<h3>Heading 3</h3>
<h4>Heading 4</h4>
<h5>Heading 5</h5>
<h6>Heading 6</h6>
</div>
<div class="cell">
<h2 id="list_types">List Types</h2>
<h3>Ordered List</h3>
<ol>
 	<li>List Item 1</li>
 	<li>List Item 2</li>
 	<li>List Item 3</li>
</ol>
<h3>Unordered List</h3>
<ul>
 	<li>List Item 1</li>
 	<li>List Item 2</li>
 	<li>List Item 3</li>
</ul>
</div>
<div class="cell">
<h2 id="block-quote">Block Quotes</h2>
<blockquote>“This stylesheet is going to help so freaking much.”
<cite>Blockquote</cite></blockquote>
</div>
</div>

<hr />

<h2 id="paragraph">Paragraph</h2>
<img class="alignleft" src="http://placehold.it/400x300" />

<strong>Lorem ipsum</strong> dolor sit amet, <a title="test link" href="#">test link</a> adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.

Lorem ipsum dolor sit amet, <em>emphasis</em> consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.

<hr />

<h2 id="form_elements">Forms</h2>
<style>input[type="email"],input[type="text"],textarea,select{border:1px solid #E1E1E1;}</style>

<form class="contact-form">
<div class="grid grid-2-col">
<div class="cell"><label for="exampleEmailInput">Your email</label>
<input id="exampleEmailInput" class="u-full-width" type="email" placeholder="test@mailbox.com" /></div>
<div class="cell"><label for="exampleRecipientInput">Reason for contacting</label>
<select id="exampleRecipientInput" class="u-full-width">
<option value="Option 1">Questions</option>
<option value="Option 2">Admiration</option>
<option value="Option 3">Can I get your number?</option>
</select></div>
</div>
<label for="exampleMessage">Message</label>
<textarea id="exampleMessage" class="u-full-width" placeholder="Hi Dave …"></textarea>
<label class="example-send-yourself-copy">
<input type="checkbox" /><span class="label-body">Send a copy to yourself</span>
</label>
<input class="button button-primary" type="submit" value="Submit" />

</form>

<hr />

<h2 id="misc">Misc Stuff – abbr, acronym, pre, code, sub, sup, etc.</h2>
Lorem <sup>superscript</sup> dolor <sub>subscript</sub> amet, consectetuer adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. <cite>cite</cite>. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. <acronym title="National Basketball Association">NBA</acronym> Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus. <abbr title="Avenue">AVE</abbr>
<pre>This is an example of using the &lt;pre&gt; tag</pre>
<code>&lt;?php echo "This is an example of using the &lt;code&gt; tag"; ?&gt;</code>';
	$new_page_template = ''; //ex. template-custom.php. Leave blank if you don't want a custom page template.
	//don't change the code bellow, unless you know what you're doing
	$page_check = get_page_by_title( $new_page_title );
	$new_page = array(
		'post_type'    => 'page',
		'post_title'   => $new_page_title,
		'post_content' => $new_page_content,
		'post_status'  => 'draft',
		'post_author'  => 1,
	);
	if ( !isset( $page_check->ID ) ) {
		$new_page_id = wp_insert_post( $new_page );
		if ( !empty( $new_page_template ) ) {
			update_post_meta( $new_page_id, '_wp_page_template', $new_page_template );
		}
	}
}