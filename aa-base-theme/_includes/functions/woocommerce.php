<?php

function aa_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'aa_add_woocommerce_support' );

/*------------------------------------------------
	Woocommerce 3 Gallery Support
---------------------------------------------------*/

function aa_woocommerce_gallery_support() {
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'aa_woocommerce_gallery_support' );

 /**
 * Remove the Woocommerce "Shop" page title
 */
function aa_hide_shop_page_title( $title ) {
   if ( is_shop() ) $title = false;
   return $title;
}
add_filter( 'woocommerce_show_page_title', 'aa_hide_shop_page_title' );

/**
 * Remove the Woocommerce tabs from the single product
 */
// function aa_remove_product_tabs($tabs) {
//     unset( $tabs['description'] );      	// Remove the description tab
//     unset( $tabs['reviews'] ); 			// Remove the reviews tab
//     unset( $tabs['additional_information'] );  	// Remove the additional information tab
//     return $tabs;
// }
// add_filter( 'woocommerce_product_tabs', 'aa_remove_product_tabs', 98 );


/**
 * Remove Reviews metafield from editor
 */
// function aa_remove_post_metaboxes() {
// 	remove_meta_box( 'commentsdiv' , 'product' , 'normal' );
// }
// add_action('admin_menu', 'aa_remove_post_metaboxes', 50);


/**
 * Remove content editor from single product
 */
// function aa_remove_product_editor() {
// 	remove_post_type_support( 'product', 'editor' );
// }
// add_action( 'init', 'aa_remove_product_editor' );

/**
 * Remove Woocommerce Breadcrumbs
 */
// function aa_remove_wc_breadcrumbs() {
//     remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
// }
// add_action( 'init', 'aa_remove_wc_breadcrumbs' );


/**
 * Remove Install Woothemes Updater notice from Dashboard
 */
// remove_action( 'admin_notices', 'woothemes_updater_notice' );


/**
 * Display 16 products per page
 */
// add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 16;' ), 20 );


/**
 * Change 'Shop' page title
 */
// function aa_wc_shop_page_title($page_title) {
//     if( 'Shop' == $page_title ) {
//         return 'All';
//     } else {
//         return $page_title;
//     }
// }
// add_filter( 'woocommerce_page_title', 'aa_wc_shop_page_title');