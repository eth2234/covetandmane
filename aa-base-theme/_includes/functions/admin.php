<?php

// Remove unnecessary links from the admin bar: WordPress logo, themes, customize
function aa_simplify_admin_bar() {
	global $wp_admin_bar;
	if ( !isset( $wp_admin_bar ) || !$wp_admin_bar instanceof WP_Admin_Bar ) return;
	
	$wp_admin_bar->remove_node( 'wp-logo' );
	$wp_admin_bar->remove_node( 'themes' );
	$wp_admin_bar->remove_node( 'customize' );
	$wp_admin_bar->remove_node( 'updates' ); // Updates
	$wp_admin_bar->remove_node( 'appearance' ); // Theme > Customize
}

add_action( 'wp_before_admin_bar_render', 'aa_simplify_admin_bar' );


// Ensures links to Visit Site and Go to Dashboard are always available.
// Adds a link for "Manage" that includes menus, widgets, plugins, users.
// Adds a link for "Content' that includes all post types.
// Replaces "Howdy," text with "Logged in as..."
function aa_improve_admin_bar( $wp_admin_bar ) {
	if ( !$wp_admin_bar instanceof WP_Admin_Bar ) return;
	
	// Display "Website" or "Dashboard" instead of the full site title on the admin bar
	$node = $wp_admin_bar->get_node( 'site-name' );
	$node->title = strpos( $node->href, 'wp-admin' ) ? 'Dashboard' : 'Website';
	$wp_admin_bar->add_node( $node );
	
	// Replace "Howdy, bob" with "Logged in as bob"
	$node = $wp_admin_bar->get_node( 'my-account' );
	$node->title = str_replace( 'Howdy, ', 'Logged in as ', $node->title );
	$wp_admin_bar->add_node( $node );
	
	// Always add a link to visit the site, even if already on the site (to return to the homepage)
	if ( !$wp_admin_bar->get_node( 'view-site' ) ) {
		$wp_admin_bar->add_menu( array(
			'id'     => 'view-site',
			'parent' => 'site-name',
			'title'  => 'Visit Site',
			'href'   => site_url(),
			'meta'   => array(
				'title' => 'Visit Website',
			),
		) );
	}
	
	// Add a link to the store
	if ( !$wp_admin_bar->get_node( 'view-store' ) && class_exists( 'woocommerce' ) ) {
		$wp_admin_bar->add_menu( array(
			'id'     => 'view-store',
			'parent' => 'site-name',
			'title'  => 'Visit Store',
			'href'   => get_permalink( wc_get_page_id( 'shop' ) ),
			'meta'   => array(
				'title' => 'Visit Store',
			),
		) );
	}
	
	// Always add a link to the dashboard, even if already on the dashboard
	if ( !$wp_admin_bar->get_node( 'dashboard' ) ) {
		$wp_admin_bar->add_menu( array(
			'id'     => 'dashboard',
			'parent' => 'site-name',
			'title'  => 'Dashboard',
			'href'   => admin_url(),
			'meta'   => array(
				'title' => 'Dashboard',
			),
		) );
	}
	
	// =======================
	// Manage
	// - Quick access to management screens: Menus, widgets, plugins, users
	
	$wp_admin_bar->add_menu( array(
		'id'    => 'management',
		'title' => 'Manage',
		'href'  => '#',
		'meta'  => array( 'title' => __( 'Manage various site settings' ), ),
	) );
	
	// Hook to add theme option pages. Used in theme-settings.php
	$manage_pages = apply_filters( 'lm-admin-menu-links', array(), $wp_admin_bar );
	
	if ( !empty( $manage_pages ) ) {
		foreach( $manage_pages as $page ) {
			$wp_admin_bar->add_node( $page );
		}
	}
	
	$wp_admin_bar->add_menu( array(
		'id'     => 'management-menus',
		'parent' => 'management',
		'title'  => 'Menus',
		'href'   => admin_url( 'nav-menus.php' ),
		'meta'   => array(
			'title' => 'Menus',
			'class' => 'post-type-node post-type-node-menus',
		),
	) );
	
	$wp_admin_bar->add_menu( array(
		'id'     => 'management-widgets',
		'parent' => 'management',
		'title'  => 'Widgets',
		'href'   => admin_url( 'widgets.php' ),
		'meta'   => array(
			'title' => 'Widgets',
			'class' => 'post-type-node post-type-node-widgets',
		),
	) );
	
	$wp_admin_bar->add_menu( array(
		'id'     => 'management-plugins',
		'parent' => 'management',
		'title'  => 'Plugins',
		'href'   => admin_url( 'plugins.php' ),
		'meta'   => array(
			'title' => 'Plugins',
			'class' => 'post-type-node post-type-node-plugins',
		),
	) );
	
	$wp_admin_bar->add_menu( array(
		'id'     => 'management-users',
		'parent' => 'management',
		'title'  => 'Users',
		'href'   => admin_url( 'users.php' ),
		'meta'   => array(
			'title' => 'Users',
			'class' => 'post-type-node post-type-node-users',
		),
	) );
	
	// =======================
	// Content
	// - Lists all post types and users dashboard pages.
	$wp_admin_bar->add_menu( array(
		'id'    => 'post-types',
		'title' => 'Content',
		'href'  => '#',
		'meta'  => array( 'title' => __( 'Manage custom post type content' ), ),
	) );
	
	// Display built-in post types: Posts, pages, media
	$args = array(
		'public'   => true,
		'show_ui'  => true,
		'_builtin' => true,
	);
	
	$all_post_types = get_post_types( $args );
	
	foreach( $all_post_types as $key => $type ) {
		$obj = get_post_type_object( $type );
		
		$wp_admin_bar->add_menu( array(
			'id'     => 'post-type-' . $key,
			'parent' => 'post-types',
			'title'  => $obj->labels->menu_name,
			'href'   => admin_url( 'edit.php?post_type=' . $key ),
			'meta'   => array(
				'title' => $obj->labels->menu_name,
				'class' => 'post-type-node post-type-node-' . $key,
			),
		) );
	}
	
	// Custom post types.
	$args = array(
		'public'   => true,
		'show_ui'  => true,
		'_builtin' => false,
	);
	
	$custom_post_types = get_post_types( $args );
	
	if ( $custom_post_types ) {
		
		// Add custom post types
		foreach( $custom_post_types as $key => $type ) {
			$obj = get_post_type_object( $type );
			
			$wp_admin_bar->add_menu( array(
				'id'     => 'post-type-' . $key,
				'parent' => 'post-types',
				'title'  => $obj->labels->menu_name,
				'href'   => admin_url( 'edit.php?post_type=' . $key ),
				'meta'   => array(
					'title' => $obj->labels->menu_name,
					'class' => 'post-type-node post-type-node-' . $key,
				),
			) );
		}
	}
}

add_action( 'admin_bar_menu', 'aa_improve_admin_bar', 60 );


// Adds a media URL column to the Media post type in list view.
function aa_media_admin_column( $cols ) {
	// Insert [media_url] into array in the 3rd position.
	$cols =
		array_slice( $cols, 0, 3, true ) +
		array( 'media_url' => 'Media URL' ) +
		array_slice( $cols, 3, count( $cols ) - 1, true );
	
	return $cols;
}

add_filter( 'manage_media_columns', 'aa_media_admin_column' );


// Display the URL and thumbnail sizes for each media item in the media screen, list view.
function aa_media_admin_value( $column_name, $id ) {
	if ( $column_name != 'media_url' ) return;
	
	$meta = wp_get_attachment_metadata( $id );
	$url = wp_get_attachment_url( wp_get_attachment_url( $id ) );
	
	if ( $url ) {
		?>
		<p>
			<input type="text" onfocus="var $me=this;setTimeout(function(){$me.select();},60);" readonly="readonly" value="<?php echo esc_attr( $url ); ?>" class="code" style="width: 100%; box-sizing: border-box; direction: rtl;">
		</p>
		<?php
	}
	
	if ( $meta ) {
		echo '<p class="description">';
		
		if ( $meta['width'] && $meta['height'] ) {
			echo sprintf( 'Original Size: %s&times;%s', $meta['width'], $meta['height'] );
		}
		
		if ( $meta['sizes'] ) {
			
			$size_array = array();
			
			foreach( $meta['sizes'] as $size => $size_meta ) {
				// Warns about undefined index "full", silence with an @ symbol.
				$src = wp_get_attachment_image_src( $id, $size );
				$sized_url = $src ? $src[0] : false;
				if ( !$sized_url || $sized_url == $url ) continue;
				
				$size_array[] = sprintf(
					'<a href="%s" target="_blank" title="Image resolution: %sx%s">%s</a>',
					esc_attr( $sized_url ),
					$size_meta['width'],
					$size_meta['height'],
					esc_html( $size )
				);
			}
			
			if ( $size_array && $meta['width'] && $meta['height'] ) echo "<br/>";
			
			if ( $size_array ) echo "Sizes: " . implode( ', ', $size_array );
		}
		
		echo '</p>';
	}
}

add_action( 'manage_media_custom_column', 'aa_media_admin_value', 10, 2 );


// Add logo to the top of the dashboard sidebar
function aa_admin_menu() {
	global $menu;
	$url = get_home_url();
	$blog_title = get_bloginfo();
	$menu[0] = array(
		__( $blog_title ),
		'read',
		$url,
		'aa-logo',
		'aa-logo',
	);
}

add_action( 'admin_menu', 'aa_admin_menu' );


// Style dashboard sidebar site logo using field from theme settings
function aa_admin_menu_logo() {
	if ( !function_exists( 'get_field' ) ) return;
	
	$admin_logo = get_field( 'admin_logo', 'options' );
	if ( !empty( $admin_logo ) ):
		?>
		<style type="text/css">
			#adminmenu a.aa-logo {
				background: url(<?php echo esc_attr($admin_logo['url']); ?>) no-repeat center center;
				background-size: 175px;
				cursor: default;
			}
			
			#adminmenu a.aa-logo div.wp-menu-name {
				display: none !important;
			}
			
			.folded #adminmenu a.aa-logo {
				background: none !important;
				padding: 0 !important;
			}
		</style>
	<?php
	endif;
}

add_action( 'admin_head', 'aa_admin_menu_logo' );


// Disable several default dashboard widgets.
function disable_default_dashboard_widgets() {
	
	// disable default dashboard widgets
	//remove_meta_box('dashboard_activity', 'dashboard', 'core');
	//remove_meta_box('dashboard_right_now', 'dashboard', 'core');
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'core' );
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );
	
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' );
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'core' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' );
	
	// disable Simple:Press dashboard widget
	remove_meta_box( 'sf_announce', 'dashboard', 'normal' );
}

add_action( 'admin_menu', 'disable_default_dashboard_widgets' );

//Remove Admin Color Scheme Picker.
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

// Customize admin footer text
function remove_footer_admin() {
	?>
	<style type="text/css">#footer-upgrade:before {
			content: "CMS ";
		}</style>
	<span id="footer-thankyou">Developed by<a href="https://alchemyandaim.com/" target="_blank"><span style="font-style:normal;margin-left:5px;"><span class="icon"><img src="https://alchemyandaim.com/wp-content/themes/brandibernoskie2014/images/favicon.png" alt="" /></span> Alchemy + Aim</span></span></a>
	<?php
}

add_filter( 'admin_footer_text', 'remove_footer_admin' );

// Customize login screen logo from ACF theme settings
function custom_login_logo() {
	if ( !function_exists( 'get_field' ) ) return;
	
	if ( $image = get_field( 'site_login_logo', 'option' ) ):
		?>
		<style type="text/css">
			h1 a { background-image:url(<?php echo esc_attr($image['url']); ?>) !important;}
		</style>
		<?php
	else:
		?>
		<style type="text/css">
			h1 a { background-image:url(<?php echo esc_attr(get_stylesheet_directory_uri()); ?>/_static/images/alchemy-and-aim-logo.png) !important;}
		</style>
		<?php
	endif;
}
add_action( 'login_head', 'custom_login_logo' );

// Customize login screen page & panel
function login_custom_stylesheet() { ?>
	<link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_bloginfo( 'stylesheet_directory' ) . '/_static/styles/_admin/login-styles.css'; ?>" type="text/css" media="all" />
<?php }
add_action( 'login_enqueue_scripts', 'login_custom_stylesheet' );

// Change  Login Logo Link
function loginpage_custom_link() {
	return get_home_url();
}
add_filter( 'login_headerurl', 'loginpage_custom_link' );

// Rename the hover logo text on the login logo
function change_title_on_logo() {
	return get_bloginfo();
}
add_filter( 'login_headertitle', 'change_title_on_logo' );

// Add custom post types count action to WP Dashboard
function aa_custom_post_type_dashboard_glance() {
	$glances = array();
	
	$args = array(
		'public'   => true,
		// Showing public post types only
		'_builtin' => false
		// Except the build-in wp post types (page, post, attachments)
	);
	
	// Getting the custom post types
	$post_types = get_post_types( $args, 'object', 'and' );
	
	foreach( $post_types as $post_type ) {
		// Counting each post
		$num_posts = wp_count_posts( $post_type->name );
		// Number format
		$num = number_format_i18n( $num_posts->publish );
		// Text format
		$text = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );
		// If use capable to edit the post type
		if ( current_user_can( 'edit_posts' ) ) {
			// Show with link
			$glance = '<a class="' . $post_type->name . '-count" href="' . admin_url( 'edit.php?post_type=' . $post_type->name ) . '">' . $num . ' ' . $text . '</a>';
		}else{
			// Show without link
			$glance = '<span class="' . $post_type->name . '-count">' . $num . ' ' . $text . '</span>';
		}
		// Save in array
		$glances[] = $glance;
	}
	
	// return them
	return $glances;
}
add_action( 'dashboard_glance_items', 'aa_custom_post_type_dashboard_glance' );