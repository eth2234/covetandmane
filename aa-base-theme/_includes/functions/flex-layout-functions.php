<?php
/**
 * These functions are used by the page template "flex-layout.php"
 */

/**
 * Generates the start tag for a custom layout section.
 *
 * @param $section
 * @param $index
 * @param array $extra_classes
 */
function aa_flex_layout_section_start( $section, $index, $extra_classes = array() ) {
	// Set the active layout, so we don't have to call $section on the closing tag.
	_aa_the_custom_section( $section );
	
	// Classes
	$classes = array( 'layout-section' );
	$classes[] = 'section-' . $section['acf_fc_layout']; // Type of section, eg "content" would be "section-content"
	$classes[] = 'text_style-' . $section['text_style']; // Text style, see core CSS
	$classes[] = 'padding-' . $section['section_padding']; // Section Padding, see core CSS
	$classes[] = 'container-' . $section['container_width']; // Container Width, see core CSS
	$classes[] = 'background_style-' . $section['background_style']; // Background style, css core CSS
	if ( !empty($section['css_classes']) ) $classes = array_merge( $classes, explode(' ', $section['css_classes']) ); // Custom CSS Classes from the Advanced tab of any section.
	$classes = array_merge( $classes, $extra_classes ); // Append custom classes with the function, for individual fields
	
	// Default ID just uses the field index. Prefer sanitized section_label, if given. Or most preferred is a css_id given under the Advanced tab.
	$id = 'section-' . $index;
	if ( !empty($section['section_label']) ) $id = strtolower( sanitize_title_with_dashes( $section['section_label'] ) );
	if ( !empty($section['css_id']) ) $id = $section['css_id'];
	
	?>
	<section class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>" id="<?php echo esc_attr( $id ); ?>">
		<?php aa_custom_section_background_start( $section ); ?>
	
		<div class="cls-inner">
	<?php
}

/**
 * Generate the end tag following aa_flex_layout_start().
 */
function aa_flex_layout_section_end() {
	$section = _aa_the_custom_section();
	?>
		</div> <!-- /cls-inner -->
		<?php aa_custom_section_background_end( $section ); ?>
	</section> <!-- /custom-section -->
	<?php
}

/**
 * Used to get and set the active section.
 *
 * @param null $maybe_section
 *
 * @return null
 */
function _aa_the_custom_section( $maybe_section = null ) {
	static $the_section = null;
	if ( $maybe_section ) $the_section = $maybe_section;
	return $the_section;
}

function aa_custom_section_background_start($section) {
	$classes = array('cls-background');
	$html_attrs = '';
	
	switch($section['background_style']) {
		case 'solid':
			$color = $section['background_color'];
			
			if ( $color ) {
				$html_attrs = 'style="background-color: '. esc_attr($color) .';"';
			}
			break;
		
		case 'gradient':
			$start = $section['background_gradient']['start_color'];
			$end = $section['background_gradient']['end_color'];
			$direction = $section['background_gradient']['direction'];
			
			if ( $start && $end && $direction ) {
				$html_attrs = 'style="background-image: linear-gradient('. esc_attr("$direction, $start, $end") .');"';
			}
			break;
			
		case 'image':
			$url = $section['background_image']['url'];
			$tint = $section['background_color_overlay'];
			$opacity = $section['background_overlay_opacity'];
			
			function hex2rgb2($hex) {
			$hex = str_replace("#", "", $hex);

			if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
			} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
			}
			$rgb = array($r, $g, $b);

			return $rgb; // returns an array with the rgb values
			}
			
			$Hex_color = $tint;
			$RGB_color = hex2rgb2($Hex_color);
			$Final_Rgb_color = implode(", ", $RGB_color);
			
			if ( $url ) {
				$html_attrs = 'style="background-image: linear-gradient(rgba('. esc_attr($Final_Rgb_color) .','. esc_attr($opacity) .'), rgba('. esc_attr($Final_Rgb_color) .','. esc_attr($opacity) .')), url('. esc_attr($url) .');"';
			}
			
			$classes[] = 'background_mobile-' . $section['background_mobile'];
			break;
	}
	
	echo '<div class="'. esc_attr(implode(' ', $classes)) .'" '. $html_attrs .'>';
	
	// Replace background image for mobile, if set
	if ( $section['background_mobile'] == 'mobile-image-background' ) {
		// Use the desktop background image as a fallback, at a smaller size
		$url = !empty($section['background_mobile_image']['url']) ? $section['background_mobile_image']['url'] : null;
		
		// Use the medium size of the background if available.
		if ( $url === null ) {
			$url = !empty($section['background_image']['sizes']['medium']) ? $section['background_image']['sizes']['medium'] : $section['background_image']['url'];
		}
		
		echo '<div class="cls-mobile-background" style="background-image: url('. esc_attr($url) .');">';
	}
}

function aa_custom_section_background_end($section) {
	// End tag if replacing background image for mobile
	if ( $section['background_mobile'] == 'mobile-image-background' ) {
		echo '</div> <!-- /cls-mobile-background -->';
	}
	
	// Append different size background image for mobile, if set
	if ( $section['background_mobile'] == 'mobile-image-after' ) {
		// Use the desktop background image as a fallback, at a smaller size
		$id = !empty($section['background_mobile_image']['ID']) ? $section['background_mobile_image']['ID'] : $section['background_image']['ID'];
		echo '<div class="cls-mobile-image">';
		echo wp_get_attachment_image($id, 'medium');
		echo '</div>';
	}
	
	// Append different size background image for mobile before the text, if set
	if ( $section['background_mobile'] == 'mobile-image-before' ) {
		// Use the desktop background image as a fallback, at a smaller size
		$id = !empty($section['background_mobile_image']['ID']) ? $section['background_mobile_image']['ID'] : $section['background_image']['ID'];
		echo '<div class="cls-mobile-image before-txt">';
		echo wp_get_attachment_image($id, 'medium');
		echo '</div>';
	}
	
	echo '</div> <!-- /cls-background -->';
}
