<?php
//BLOCK REGISTERY 
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	//CHECK IF ACF BLOCK TYPE FUNCTION EXISTS
	if( function_exists('acf_register_block_type') ) {
		
		//ADD A+A BLOCK CATEGORY
		function custom_block_category( $categories ) {
				$custom_block = array(
					'slug' => 'aa-blocks',
					'title' => __( 'Alchemy + Aim Blocks', 'aa-blocks' ),
			);
				$categories_sorted = array();
				$categories_sorted[0] = $custom_block;
				foreach ($categories as $category) {
					$categories_sorted[] = $category;
			}
				return $categories_sorted;
			}
			add_filter( 'block_categories', 'custom_block_category', 10, 2);
		
		//REGISTER BLOCKS
		if ( have_rows( 'blocks', 'option' ) ) : 
			while ( have_rows( 'blocks', 'option' ) ) : the_row(); 
			//var
			$name = get_sub_field( 'block_name' ); 
			$title = get_sub_field( 'block_title' ); 
			$desc = get_sub_field( 'block_description' );
			$preview = get_sub_field( 'block_preview_image' );
			$icon_type = get_sub_field( 'custom_icon' );
			$dash_icon = get_sub_field( 'block_icon' );
			$custom_icon = get_sub_field( 'block_custom_icon' );
			$cat = get_sub_field( 'block_category' );
			$key_words = get_sub_field( 'block_keywords' );
			$scripts = get_sub_field('block_scripts');

			if( $icon_type == 1 )  {
				$icon = ''.$custom_icon.'';
			} else {
				$icon = $dash_icon;
			}
		
		$args = array(
			'name'				=> $name,
			'title'				=> __($title),
			'description'		=> __($desc),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> $cat,
			'icon'				=> $icon,
			'mode' 				=> 'preview',
			'keywords'			=> array( $key_words ),
			'example' => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'gutenberg_preview_img' => __('<img src="'.$preview.'">'),
					),
				)
			)
			);
		
		//INSERT ENQUEUED SCRIPTS & STLYES IF APPLICABLE
		if($scripts){
			//$args['enqueue_assets'] = function(){
		if ( have_rows( 'block_scripts' ) ) :
			while ( have_rows( 'block_scripts' ) ) : the_row();
				if ( function_exists('wp_enqueue_script') ) {
					//var	
					$type = get_sub_field( 'script_style' );
					$name = get_sub_field( 'short_name' );
					$url = get_sub_field( 'url' ); 
					$ver = get_sub_field( 'version_number' );
					
					if($type == "wp_enqueue_script"){
						call_user_func( $type, $name, $url, array(), $ver, true );
					}else {
						call_user_func( $type, $name, $url, array(), $ver );	
					}
					
				}else{
					echo '<!-- Cannot enqueue asset: "'. esc_html($name) .'" (function does not exist) -->';
				}

			endwhile; 
			else : 
			endif;
			//};
		}

		acf_register_block_type( $args );
		

	 		endwhile; 
		else : 
			 // no rows found 
		endif;

	}
}

//FUNCTION TO CONVERT THE BACKGROUND HEX COLOR TO RGB
function hex2rgb($hex) {
	$hex = str_replace("#","", $hex);

	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}
		$rgb = array($r, $g, $b);

		return $rgb; // returns an array with the rgb values
	}

//RENDER BLOCKS
function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/block-name") into path friendly slug ("block-name")
	$slug = str_replace('acf/', '', $block['name']); 
	
	// create id attribute for specific styling
	$id = $slug . '-' . $block['id'];

	// create align class ("alignwide") from block setting ("wide")
	$align_class = $block['align'] ? 'align' . $block['align'] : '';

	//var
	$txt_style = get_field( 'text_style' );
	$padding = get_field( 'section_padding' );
	$container = get_field( 'container_width' ); 
	$bg_style = get_field( 'background_style' );
	$bg_clr = get_field( 'background_color' );
	$label = get_field( 'section_label' );
	$id_custom = strtolower( sanitize_title_with_dashes( $label ) );

?>

<section class="layout-section section-<?php echo $slug; ?> <?php echo $block['className']; ?> <?php echo $align_class; ?> text_style-<?php echo $txt_style; ?> padding-<?php echo $padding; ?> container-<?php echo $container; ?> background_style-<?php echo $bg_style; ?>" id="<?php if( !empty($label) ){ echo $id_custom; } else { echo $id; } ?>">
	<div class="gutenberg-preview-img"><?php the_field('gutenberg_preview_img'); ?></div>
		<?php if ( $bg_style == 'solid' ) { ?>
	<div class="cls-background" style="background-color: <?php echo $bg_clr; ?>;">
		<?php } else if  ( $bg_style == 'gradient') { ?>
			<?php if ( have_rows( 'background_gradient' ) ) : ?>
			<?php while ( have_rows( 'background_gradient' ) ) : the_row(); ?>
			<?php 	//var
					$start_clr = get_sub_field( 'start_color' );
					$end_clr = get_sub_field( 'end_color' );
					$clr_dir = get_sub_field( 'direction' );
			?>
	<div class="cls-background" style="background-image: linear-gradient(<?php echo $clr_dir; ?>, <?php echo $start_clr; ?>, <?php echo $end_clr; ?>);">
			<?php endwhile; ?>
			<?php endif; ?>
			<?php } else if  ( $bg_style == 'image' ) {?>
			<?php 	//var
					$bg_image = get_field( 'background_image' );
					$bg_overlay_tint = get_field( 'background_color_overlay' );
					$bg_opacity = get_field( 'background_overlay_opacity' );
					$bg_mobile = get_field( 'background_mobile' );
					$bg_mobile_img = get_field( 'background_mobile_image' );
		
			$Hex_color = $bg_overlay_tint;
			$RGB_color = hex2rgb($Hex_color);
			$bg_overlay = implode(",", $RGB_color);
		
			?>
	<div class="cls-background background_<?php if ( $bg_mobile == 'mobile-image-background' || $bg_mobile == 'mobile-image-after' || $bg_mobile == 'mobile-image-before' ) { ?>mobile-<?php } ?><?php echo $bg_mobile; ?>" style="background-image: linear-gradient(rgba(<?php echo $bg_overlay; ?>,<?php echo $bg_opacity; ?>),rgba(<?php echo $bg_overlay; ?>,<?php echo $bg_opacity; ?>)),url(<?php echo $bg_image['url']; ?>);">
			<?php } ?>
				
		<?php if ( $bg_mobile == 'mobile-image-background' ) { ?>
		<div class="cls-mobile-background"  style="background-image: linear-gradient(<?php echo $bg_overlay; ?>,<?php echo $bg_overlay; ?>),url(<?php echo $bg_mobile_img['url']; ?>);">
		<?php } ?>
					
		<?php if ( $bg_mobile == 'mobile-image-before' ) {?>
		<div class="cls-mobile-image before-txt"><img src="<?php echo $bg_mobile_img['url']; ?>" alt="<?php echo $bg_mobile_imge['alt']; ?>" /> </div>
		<?php } ?>	
					
			<div class="cls-inner">
	
			<?php	// include a template part from within the "template-parts/block" folder
				if( file_exists( get_theme_file_path("/_template-parts/acf-blocks/block/{$slug}.php") ) ) {
					include( get_theme_file_path("/_template-parts/acf-blocks/block/{$slug}.php") );
				} ?>
				
			</div>
			
		<?php if ( $bg_mobile == 'mobile-image-background' ) {?></div><?php } ?>
		<?php if ( $bg_mobile == 'mobile-image-after' ) {?>
		<div class="cls-mobile-image"><img src="<?php echo $bg_mobile_img['url']; ?>" alt="<?php echo $bg_mobile_imge['alt']; ?>" /> 
		<?php } ?>
			
	</div>
</section>
<?php } ?>