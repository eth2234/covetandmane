<?php

// Add typekit to visual editor
function aa_include_editor_js( $plugin_array ) {
	$plugin_array['typekit'] = get_template_directory_uri() . '/_static/js/editor.js';
	return $plugin_array;
}
add_filter( 'mce_external_plugins', 'aa_include_editor_js' );

// Add a format select to the visual editor top bar, after the first item which should be the formatting dropdown.
function aa_enable_editor_format_select($buttons) {
	return array_merge( array_slice($buttons, 0, 1, true) + array('ss' => 'styleselect', 'fss' => 'fontsizeselect') + array_slice($buttons, 1, null, true) );
}
add_filter('mce_buttons', 'aa_enable_editor_format_select');

function aa_custom_font_size_intervals($tmce_args) {
	$tmce_args['fontsize_formats'] = "18px 20px 22px 25px 35px 40px 55px 72px";
	return $tmce_args;
}
add_filter( 'tiny_mce_before_init', 'aa_custom_font_size_intervals' );

// Include custom formats in the format dropdown
function aa_insert_editor_formats( $init_array ) {
	/* Default Style Formats */
	$style_formats = array(
		array(
			'title'   => 'Colors',
			'items' => array(
				array(
					'title'   => 'Black',
					'inline' => 'span',
					'classes'  => 'color-black', // 000000
				),
				array(
					'title'   => 'White',
					'inline' => 'span',
					'classes'  => 'color-white', // ffffff
				),
			),
		),
		array(
			'title'   => 'Font Weight',
			'items' => array(
				array(
					'title'   => 'Light',
					'inline' => 'span',
					'classes'  => 'weight-light', // 300
				),
				array(
					'title'   => 'Regular',
					'inline' => 'span',
					'classes'  => 'weight-regular', // 400
				),
				array(
					'title'   => 'Medium',
					'inline' => 'span',
					'classes'  => 'weight-medium', // 500
				),
				array(
					'title'   => 'Bold',
					'inline' => 'span',
					'classes'  => 'weight-bold', // 700
				),
				array(
					'title'   => 'Black',
					'inline' => 'span',
					'classes'  => 'weight-black', // 900
				),
			),
		),
		array(
			'title'   => 'Links',
			'items' => array(
				array(
					'title' => 'Ghost Button',
					'selector' => 'a',
					'classes' => 'button',
					'exact' => true,
				),
				array(
					'title' => 'Primary Button',
					'selector' => 'a',
					'classes' => 'button button-primary',
					'exact' => true,
				),
			),
		),
		array(
			'title'   => 'Inline',
			'items' => array(
				array(
					'title'   => 'Underline',
					'icon'    => 'underline',
					'inline' => 'span',
					'classes'  => 'text-underline',
				),
				array(
					'title'   => 'Strikethrough',
					'format'  => 'strikethrough',
					'icon'    => 'strikethrough',
				),
				array(
					'title'   => 'Superscript',
					'format'  => 'superscript',
					'icon'    => 'superscript',
				),
				array(
					'title'   => 'Subscript',
					'format'  => 'subscript',
					'icon'    => 'subscript',
				),
				array(
					'title'   => 'Code',
					'format'  => 'code',
					'icon'    => 'code',
				),
			),
		),
	);
	
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );
	
	return $init_array;
	
}
add_filter( 'tiny_mce_before_init', 'aa_insert_editor_formats' );