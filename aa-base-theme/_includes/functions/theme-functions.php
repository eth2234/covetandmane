<?php

// Used in comments.php to display the list of comments.
function aa_list_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment ?>
	<li <?php comment_class( 'comment' ); ?> id="comment-<?php comment_ID() ?>">
		<div id="div-comment-<?php comment_ID(); ?>" class="comments">
			<p class="comments-block">
			<div class="user"><?php echo get_avatar( $comment, 96 ); ?></div>
			<div class="message">
				<div class="reply-link"><?php comment_reply_link( array_merge( $args, array(
						'add_below' => 'div-comment',
						'depth'     => $depth,
						'max_depth' => $args['max_depth'],
					) ) ) ?></div>
				<div class="info">
					<div class="comment-author"><?php comment_author_link() ?> </div>
					<div class="comment-time comment-meta commentmetadata">
						<a class="date" href="#comment-<?php comment_ID(); ?>" title="Permanent Link to this comment"><?php comment_date( 'F j, Y' );
							echo " AT "; ?><?php comment_time( 'g:iA' ); ?></a> <?php edit_comment_link( 'Edit', '  (', ')' ); ?></div>
					</p>
					<div class="comment-body">
						
						<?php comment_text() ?>
						<?php if ( $comment->comment_approved == '0' ) : ?>
							<p>Thank you. Comments are moderated. Your comment will appear shortly.</p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		
		</div>
	</li>
	<?php
}

// Open comment author links in a new window
add_filter( 'get_comment_author_link', 'open_comment_author_link_in_new_window' );
function open_comment_author_link_in_new_window( $author_link ) {
    return str_replace( "<a", "<a target='_blank'", $author_link );
}

// Get the sidebar to be used on the current page
function aa_get_sidebar() {
	$area = 'sidebar';
	
	if ( is_front_page() ) $area = 'front-page';
	else if ( is_search() ) $area = 'search';
	else if ( is_home() || is_post_type_archive('post')|| is_tax('category') || is_tax('post_tag') || get_post_type() == 'post' ) $area = 'blog';
	
	if ( class_exists('WooCommerce') ) {
		if ( is_woocommerce() || is_shop() || is_post_type_archive( 'product' ) ) $area = 'store';
		else if ( is_tax( 'product_cat' ) ) $area = 'store';
		else if ( is_tax( 'product_tag' ) ) $area = 'store';
		
		// Checkout and cart page
		if ( get_the_ID() == get_option('woocommerce_cart_page_id') ) $area = 'checkout';
		else if ( get_the_ID() == get_option('woocommerce_checkout_page_id') ) $area = 'checkout';
	}
	
	// If a specific sidebar is empty, fall back to the default
	if ( $area !== 'sidebar' && !is_active_sidebar($area) ) $area = 'sidebar';
	
	// If still empty, we might return false.
	return is_active_sidebar($area) ? $area : false;
}