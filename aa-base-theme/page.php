<?php
get_header();
?>
	<div class="container">
    	<div class="content">
	
			<?php
			while( have_posts() ): the_post();
				
				get_template_part( '_template-parts/loop', get_post_type() );
			
			endwhile;		
			?>
			
		</div>
	</div>
<?php
get_footer();