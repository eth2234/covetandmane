<?php
// Do not access this file directly
if ( !defined('ABSPATH') ) exit;

// If passworded, must have given password
if ( post_password_required() ) return;

// If comments are closed and there are no comments already, do not show comments at all
if ( !comments_open() && !get_comments_number() ) return;
?>
<div id="comments">
	
	<h4>
		<?php comments_number( '0 comments to', '1 comment to', '% comments to' ); ?>
		"
		<?php the_title(); ?>
		"</h4>
	<?php if ( have_comments() ) { ?>
		<ul class="comment-block" id="comment-block">
			<?php
			// Callback defined in /_includes/functions/theme-functions.php
			wp_list_comments( 'callback=aa_list_comments' );
			?>
		</ul>
	<?php } ?>
	
	<!-- Comment Form -->
	<?php if ( 'open' == $post->comment_status ) : ?>
		
		<div id="respond">
			
			<h4 class="comments-headers"><?php comment_form_title( 'Leave a Comment', 'Leave a Reply to %s' ); ?>
				<span id="cancel-comment-reply"><?php cancel_comment_reply_link( '(cancel)' ) ?></span></h4>
			
			<?php if ( get_option( 'comment_registration' ) && !$user_ID ) : ?>
				
				<p class="unstyled">You must <a href="<?php echo get_option( 'siteurl' ); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">log in</a> to post a comment.</p>
			
			<?php else : ?>
				
				<form action="<?php echo get_option( 'siteurl' ); ?>/wp-comments-post.php" method="post" id="comment_form">
					<?php if ( $user_ID ) { ?>
						<p class="unstyled">Logged in as <a href="<?php echo get_option( 'siteurl' ); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.
							<a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="<?php _e( 'Log out of this account' ) ?>">Logout &raquo;</a></p>
					<?php } ?>
					<?php if ( !$user_ID ) { ?>
						<input class="u-full-width" type="text" name="author" id="author" value="<?php echo $comment_author; ?>" tabindex="1" placeholder="Name" />
						<input class="u-full-width" type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" tabindex="2" placeholder="Email" />
						<input class="u-full-width" type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" tabindex="3" placeholder="Website" />
					<?php } ?>
					<textarea class="u-full-width" name="comment" id="comment" rows="8" tabindex="4" placeholder="Comment"></textarea>
					<?php if ( function_exists( 'show_subscription_checkbox' ) ) {
						show_subscription_checkbox();
					} ?>
					<?php comment_id_fields(); ?><input name="submit" class="button-primary" type="submit" id="submit" tabindex="5" value="Submit" />
					<?php do_action( 'comment_form', $post->ID ); ?>
				</form>
			
			<?php endif; // If registration required and not logged in ?>
		
		</div>
		
		<div class="pagination pagination-comments">
			<div class="nav-previous"><?php previous_comments_link( 'older comments' ) ?></div>
			<div class="nav-next"><?php next_comments_link( 'newer comments' ) ?></div>
		</div>
	
	<?php endif; // if you delete this the sky will fall on your head ?>

</div><?php /* #comments */ ?>
<div style="clear:both;"></div>
