//Add TypeKit to Visual Editor ()
/*(function() {
	tinymce.create('tinymce.plugins.typekit', {
		init: function(ed, url) {
			ed.onPreInit.add(function(ed) {
				// Get the DOM document object for the IFRAME
				var doc = ed.getDoc();
				var jscript = "(function() { 
          var config = { 
            kitId: 'TypeKitID' 
          }; 
          var d = false; 
          var tk = document.createElement('script'); 
          tk.src = '//use.typekit.net/' + config.kitId + '.js'; 
          tk.type = 'text/javascript'; 
          tk.async = 'true'; 
          tk.onload = tk.onreadystatechange = function() { 
            var rs = this.readyState; 
            if (d || rs && rs != 'complete' && rs != 'loaded') return; 
            d = true; 
            try { Typekit.load(config); } catch (e) {} 
          }; 
         var s = document.getElementsByTagName('script')[0]; 
         s.parentNode.insertBefore(tk, s); 
       })();";

				// Create a script element and insert the TypeKit code into it
				var script = doc.createElement("script");
				script.type = "text/javascript";
				script.appendChild(doc.createTextNode(jscript));

				// Add the script to the header
				doc.getElementsByTagName('head')[0].appendChild(script);

				console.log(script);

			});
		}
	});
	tinymce.PluginManager.add('typekit', tinymce.plugins.typekit);
})();*/