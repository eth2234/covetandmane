/*  -------------------------------------------------------
	Javascript helper document insert any simple JS functions here.
------------------------------------------------------- */





/*  -------------------------------------------------------------
		DEFAULT JS FUNCTIONS BELOW THIS LINE
------------------------------------------------------------- */
/*SLIDE OUT MENU*/	
jQuery(function($) {
		// Slideout Menu
	"use strict";
    $('#slideout-trigger').on('click', function(event){
    	event.preventDefault();
    	// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250);
    	}
    });

		$('#nav-close').on('click', function(event){
			event.preventDefault();
			// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250);
    	}
    });
	
	$('#content-wrapper').on('click', function(){
			// create menu variables
			var slideoutMenu = $('#slideout-menu');
			var slideoutMenuWidth = $('#slideout-menu').outerWidth();

			// toggle open class
			slideoutMenu.removeClass("open");

			// slide menu
			slideoutMenu.animate({
					right: -slideoutMenuWidth
				}, 250);
					
    });
	
	/*SLIDE OUT MENU DROPDOWN*/	
	$('#slideout-nav .sub-menu').before('<div id="submenu-link"></div>');
    $('#slideout-menu ul ul').hide();
    if ($('#slideout-menu .menu-item-has-children').length > 0) {
        $('#slideout-menu .menu-item-has-children').click(

        function () {
            $(this).addClass('toggled');
            if ($(this).hasClass('toggled')) {
                $(this).children('ul').slideToggle();
            }
            //return false;

        });
    }
});


//Gutenburg 'align-wide' & 'align-full' on pages with sidebars
jQuery(function($) {
	if ($('.sidebar').length) { 
		$('body').addClass('has-sidebar');
	} else {
		$('body').addClass('no-sidebar');
	}
});
  
//Scrolling Anchor Link
var scroll = new SmoothScroll('a[href*="#"]');

//Move Text Below Mobile Image Flex Layout
jQuery(function($) {
	"use strict";
		$(".cls-mobile-image.before-txt").each(function() {
    		$(this).closest(".layout-section").find(".background_mobile-mobile-image-before .cls-inner").prepend(this);
		});
	});

//Fancy Credits
jQuery(function($) {
	"use strict";
$("#credit-trigger").on( "click", function(e) {
		e.preventDefault();
		console.log('click');
		if ($('#credit-trigger').hasClass("credit-open") || $('#credit-close').hasClass("credit-open")) {
			$('#credit-trigger').removeClass("credit-open");
			$('#credit-close').removeClass("credit-open");
			$("#creditslide").removeClass("credit-open");
		} else {
			$('#credit-trigger').addClass("credit-open");
			$('#credit-close').addClass("credit-open");
			$("#creditslide").addClass("credit-open");
			$("#creditslide").show();
		}
	});
	
	$("#credit-close").on( "click", function(e) {
		e.preventDefault();
		console.log('click');
		if ($('#credit-trigger').hasClass("credit-open") || $('#credit-close').hasClass("credit-open")) {
			$('#credit-trigger').removeClass("credit-open");
			$('#credit-close').removeClass("credit-open");
			$("#creditslide").removeClass("credit-open");
		} else {
			$('#credit-trigger').addClass("credit-open");
			$('#credit-close').addClass("credit-open");
			$("#creditslide").addClass("credit-open");
			$("#creditslide").show();
		}
	});
	
});	