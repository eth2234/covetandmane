// Admin JavaScript Document

// Initialize the labels for each layout in the Flex Layout page template. This isn't in a jQuery ready statement because we need to hook into ACF earlier as of v5.7.0
if ( typeof adminpage !== "undefined" && adminpage === "post-php" ) {
	let waitforacf = setInterval(function() {
		if ( typeof acf === "object" ) {
			init_post_body_flexible_field_labels();
			clearInterval( waitforacf );
		}
	}, 500);
}

function init_post_body_flexible_field_labels() {
	let initialized_class = 'aa-sl-init';
	let layout_selector = 'div.layout';
	let label_selector = 'div.acf-field.acf-field-5b243f91dca97';

	// Determine if the element is a layout and initialize it if it is a layout, or initialize layouts within if it isn't one.
	let initialize_maybe_layout = function( $e ) {
		// Only initialize once
		if ( $e.hasClass( initialized_class ) ) return;

		$e.addClass( initialized_class );

		// Initialize the layout, or layouts within
		if ( $e.is( layout_selector ) ) {
			initialize_layout( $e );
		}else{
			$e.find( layout_selector ).each(function() {
				initialize_layout( jQuery(this) );
			} );
		}
	};

	// Initialize a specific layout
	let initialize_layout = function( $layout ) {
		// Only initialize once
		if ( $layout.hasClass( initialized_class ) ) return;
		$layout.addClass( initialized_class );

		let $label_field = $layout.find( label_selector );
		let $input = $label_field.find('input[type="text"]');

		$layout.addClass( 'aa-layout-with-section-label' );
		$input.addClass('aa-section-label-input');

		$layout.prepend( jQuery('<div>').addClass('aa-section-label').append($input) );

		$label_field.detach();
	};

	// When acf is ready, initialize the flexible container. This makes all existing flexible fields get initialized.
	let acf_event_initialize_flex = function($e) {
		initialize_maybe_layout( $e );
	};

	acf.add_action('ready', acf_event_initialize_flex );
	acf.add_action('append', acf_event_initialize_flex );
}